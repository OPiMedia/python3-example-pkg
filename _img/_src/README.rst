.. -*- restructuredtext -*-

=====================
Attribution of images
=====================

* Generic, package icon:
  https://www.iconfinder.com/icons/118888/generic_package_icon
* Python Generic Application Logo:
  https://www.openclipart.org/detail/213925/python-generic-application-logo
