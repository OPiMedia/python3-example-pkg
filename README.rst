.. -*- restructuredtext -*-

===================
Python3 example pkg
===================
Small example of Python 3 package.

|



Installation
============
If pip_ is installed on your platform you can do:

.. code-block:: sh

   $ python3 -m pip install --index-url https://test.pypi.org/simple/ example_pkg_opi

Without pip, download the archive ``example_pkg_opi-?.tar.gz``, unzip it ``somewhere``.
Next in the ``somewhere/example_pkg_opi-?/`` subdirectory run:

.. code-block:: sh

   $ python3 setup.py install

In both cases, you must use **admin access**. So with GNU/Linux you will probably do:

.. code-block:: sh

   $ sudo [your command]


Or you can install in user environment with:

.. code-block:: sh

   $ python3 -m pip install --index-url https://test.pypi.org/simple/ example_pkg_opi --user

.. _pip: https://pypi.org/project/pip

|



Links
=====
`Online HTML documentation`_ on **Read The Docs**.

| **Sources** and installers on Bitbucket: https://bitbucket.org/OPiMedia/python3-example-pkg
| and on **TestPyPI**: https://test.pypi.org/project/example-pkg-opi .

.. _`Online HTML documentation`: https://python3-example-pkg.readthedocs.io/

|



Python documentation on package and distributing projects
=========================================================
* Tutorials / Installing Packages / Ensure you can run pip from the command line:
  https://packaging.python.org/tutorials/installing-packages/#ensure-you-can-run-pip-from-the-command-line
* Tutorials / Packaging Python Projects:
  https://packaging.python.org/tutorials/packaging-projects/

* Guides / Packaging and distributing projects:
  https://packaging.python.org/guides/distributing-packages-using-setuptools/
* Guides / Using TestPyPI:
  https://packaging.python.org/guides/using-testpypi/

* How To Package Your Python Code:
  https://python-packaging.readthedocs.io/

* My web page about Python:
  http://www.opimedia.be/DS/Python/

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2019-2021 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png



|pkg|

Logo composed from these two images:

* Generic, package icon:
  https://www.iconfinder.com/icons/118888/generic_package_icon
* Python Generic Application Logo:
  https://www.openclipart.org/detail/213925/python-generic-application-logo

.. |pkg| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Apr/11/3270972977-4-python3-example-pkg-logo_avatar.png

|



Changes
=======
* 1.0.15 — October 17, 2021: Corrected use of ``Pyre``, adapted string formatting, and removed link to Framasphere.
* 1.0.14 — December 27, 2020: Added ``Pyre`` use in ``Makefile``.
* 1.0.13 — April 30, 2020: Added ``Pyreverse`` use in ``Makefile``.
* 1.0.12 — April 26, 2020: Corrected documentation links in ``Makefile`` and order import in ``setup.py``.
* 1.0.11 — April 14, 2020: Cleaning for use of ``JOB`` variable in ``Makefile`` and more recent ``pydocstyle``.
* 1.0.10 — April 13, 2020: Replace old ``pep8`` static analyzer by new ``pycodestyle``.
* 1.0.9 — April 13, 2020: Minor changes about documentation, pydocstyle and parallel static checking.
* 1.0.8 — April 12, 2020: Corrected ``README.rst`` reading in ``setup.py``.
* 1.0.7 — April 11, 2020

  - Added some static tests.
  - Updated installation information.

* 1.0.6 — September 10, 2019

  - Added pydostyle use.
  - Removed import of pytest in test files.

* 1.0.5 — February 11, 2019: Improved package building and doc, and minor corrections.
* 1.0.0 — February 10, 2019: First public version
