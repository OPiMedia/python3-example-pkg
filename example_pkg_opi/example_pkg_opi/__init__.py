# -*- coding: utf-8 -*-

"""
example_pkg_opi/__init__.py.

Small example of Python 3 package.

:license: GPLv3 --- Copyright (C) 2019-2021 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: October 17, 2021
"""

name = 'example_pkg_opi'  # pylint: disable=invalid-name
"""Name of the package."""

VERSION = '1.0.15'
"""Version of the package."""

URL_DOC = 'https://python3-example-pkg.readthedocs.io/'
"""URL of the online documentation."""

URL_PYPI = 'https://test.pypi.org/project/example-pkg-opi'
"""URL of the package on TestPyPI."""

URL_SRC = 'https://bitbucket.org/OPiMedia/python3-example-pkg'
"""URL of the complete sources on Bitbucket."""

URL: str = URL_DOC
"""Main URL of the project."""


#
# Import all modules
####################
from .module import example  # noqa  # pylint: disable=wrong-import-position

assert example  # to avoid warning with pyflakes


#
# GPLv3
# ------
# Copyright (C) 2019-2021 Olivier Pirson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
