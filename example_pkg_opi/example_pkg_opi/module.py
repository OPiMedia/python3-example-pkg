# -*- coding: utf-8 -*-

"""
example_pkg_opi/module.py.

:license: GPLv3 --- Copyright (C) 2019-2021 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: October 17, 2021
"""


def example(text: str) -> str:
    """
    Return a message containing ``text``.

    Simple example function.

    :param text:
    """
    return f'Example: {text}'
