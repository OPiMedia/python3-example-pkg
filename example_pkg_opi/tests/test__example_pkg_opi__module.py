#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test__example_pkg_opi__module.py.

Test example_pkg_opi/module.py.

:license: GPLv3 --- Copyright (C) 2019, 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: December 27, 2020
"""

import test_common

import example_pkg_opi


def test__example() -> None:
    """Test `example()`."""
    assert example_pkg_opi.example('text') == 'Example: text'


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing).
    test_common.run_tests(locals())  # type: ignore
