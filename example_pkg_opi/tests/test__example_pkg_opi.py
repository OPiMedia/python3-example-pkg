#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test__example_pkg_opi.py.

Test example_pkg_opi/__init__.

:license: GPLv3 --- Copyright (C) 2019, 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: December 27, 2020
"""

import test_common

import example_pkg_opi


def test__constants() -> None:
    """Test global constants."""
    assert isinstance(example_pkg_opi.URL, str)
    assert example_pkg_opi.URL != ''

    assert isinstance(example_pkg_opi.URL_DOC, str)
    assert example_pkg_opi.URL_DOC != ''

    assert isinstance(example_pkg_opi.URL_PYPI, str)
    assert example_pkg_opi.URL_PYPI != ''

    assert isinstance(example_pkg_opi.URL_SRC, str)
    assert example_pkg_opi.URL_SRC != ''

    assert isinstance(example_pkg_opi.VERSION, str)
    assert example_pkg_opi.VERSION != ''


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing).
    test_common.run_tests(locals())  # type: ignore
