# -*- coding: utf-8 -*-

"""
test_common.py.

Common code for tests.

:license: GPLv3 --- Copyright (C) 2019-2021 Olivier Pirson
:author: Olivier Pirson --- http://www.opined.be/
:version: October 17, 2021
"""

import sys

from typing import Callable, Dict


def check_assert(exception: BaseException,
                 function: Callable[[], None]) -> None:
    """
    Check if `function` raise `exception`.

    Run `function` and check if it raise the `exception`.
    If not raise an *assertion*.

    :param exception:
    :param function:

    :exception: assertion
    """
    try:
        function()

        assert False
    except exception:  # type: ignore
        pass


def not_covered() -> None:
    """Print message to mark not covered function."""
    print('NOT covered', end=' ')


def run_tests(names: Dict[str, Callable[[], None]]) -> None:
    """
    Run all `test__...()` functions (useful if pytest is missing).

    :param names:
    """
    names = dict(names)
    length = len([None for name in names if name.startswith("test__")])
    print(f'Collected {length} items')
    for name in sorted(names):
        if name.startswith('test__'):
            print(f'{sys.argv[0]}::{name} ', end='')
            sys.stdout.flush()
            names[name]()
            sys.stdout.flush()
            sys.stderr.flush()
            print()
    print('===== done =====')
