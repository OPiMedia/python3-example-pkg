#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Install for example_pkg_opi package.

:license: GPLv3 --- Copyright (C) 2019, 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2020
"""

import setuptools  # type: ignore

import example_pkg_opi


with open('README.rst', encoding='UTF-8') as fin:
    LONG_DESCRIPTION = fin.read()

# https://packaging.python.org/guides/distributing-packages-using-setuptools/#setup-args
# https://setuptools.readthedocs.io/en/latest/setuptools.html#metadata
# https://setuptools.readthedocs.io/en/latest/setuptools.html#new-and-changed-setup-keywords
setuptools.setup(
    name=example_pkg_opi.name,
    version=example_pkg_opi.VERSION,

    description='Small example of Python 3 package',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/x-rst',

    url=example_pkg_opi.URL,

    author='🌳 Olivier Pirson — OPi 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬',
    author_email='olivier.pirson.opi@gmail.com',

    license='GPLv3',

    classifiers=(
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
    ),

    platforms=('any', ),

    keywords=('example', 'package'),

    project_urls={
        'Documentation': example_pkg_opi.URL_DOC,
        'Funding': 'http://www.opimedia.be/donate/',
        'Say Thanks!': 'http://www.opimedia.be',
        'Source': example_pkg_opi.URL_SRC,
    },

    packages=setuptools.find_packages(),

    python_requires='>=3.5',
)
